<?php
// Get current year
$currentDate = date( 'l, m-d-y');  
$currentTime = date("h:i:sa");   
$year = date('Y');

?>


<footer class="w3-padding-xlarge w3-center" style="width: 100%; height: 100%; margin-top: 0px; background-color: #091747; color: white;">
<p style="text-align: center;">J.W. Lewis Construction &copy; <?php echo $year ?>, All Rights Reserved</p>
       <div style="padding: 10px 0 10 10;">
           
  <div class="contact-email" style="text-align: center; padding-right: 120px;">
<a href="mailto: johnyc241@gmail.com"><i class="fas fa-envelope fa-2x" style="color: white;"></i></a></div> 
          

<div class="contact-linkedin" style="text-align: center; margin-top: -2px; margin-top: -55px; "> <a href="https://www.linkedin.com/in/johncurtisjr/"><i class="fab fa-linkedin-in fa-2x" style="color: white;"></i> </a></div> 
       
<div style=" padding-left: 120px; margin-top: -55px; text-align: center;">
 <a href="Tel: 1112223333" style="color: white;"><i class="fas fa-phone fa-2x"></i></a></div>  
</div>	   

</footer>