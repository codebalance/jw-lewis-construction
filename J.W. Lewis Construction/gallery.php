<!DOCTYPE html>
<html>
<head>
<!--Required Meta Tags-->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>J.W. Lewis Construction | Gallery</title>

<link rel="shortcut icon" type="image/jpg" href="Images/J.W.Lewis_Construction_logo.jpg"> 

 
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> 
   <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  -->

   <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">

   <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
   <link rel="stylesheet" href="CSS/lightbox.css"/>
   <link rel="stylesheet" href="CSS/style.css"> 

   <!--Google Fonts-->
   <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto%7CJosefin+Sans:100,300,400,500"
    rel="stylesheet" type="text/css">
	
  
	<style>
		.gallery {
    -webkit-column-count: 3;
    -moz-column-count: 3;
    column-count: 3;
    -webkit-column-width: 33%;
    -moz-column-width: 33%;
    column-width: 33%; }
    .gallery .pics {
    -webkit-transition: all 350ms ease;
    transition: all 350ms ease; }
    .gallery .animation {
    -webkit-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1); }
    
    @media (max-width: 450px) {
    .gallery {
    -webkit-column-count: 1;
    -moz-column-count: 1;
    column-count: 1;
    -webkit-column-width: 100%;
    -moz-column-width: 100%;
    column-width: 100%;
    }
    }
    
    @media (max-width: 400px) {
    .btn.filter {
    padding-left: 1.1rem;
    padding-right: 1.1rem;
    }
	}
	
.topnav {
  overflow: hidden;
}

.topnav a {
  float: right;
  display: block;
  text-align: center;
  text-decoration: none;
  padding-top: 5px;
  padding-right: 5px;
  margin-left: 5px;
  margin-right: 10px;
}

.topnav a:hover {
	text-decoration: none;
	color: #8A090B;
}


.topnav .icon {
  display: none;
}

@media screen and (max-width: 700px) {
  .topnav a:not(:first-child) {display: none;}
  .topnav a.icon {
    float: right;
    display: block;
  } 
  
	.topnav a {
		display: none;
	}
}
		
		@media screen and (max-width: 1100px) {
			.gallery-heading h1 {
				padding-left: 0%;
				padding-right: 13%;
			}
		}

@media screen and (max-width: 700px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
	float: right;
    right: 0;
    top: 0;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
	 margin-top: -25px;
  }
}
								
#return-to-top {
    position: fixed;
	z-index: 2;
    bottom: 20px;
    right: 20px;
    background: #091747;
	background: rgba(0, 0, 0, 7);
    width: 50px;
    height: 50px;
    display: block;
    text-decoration: none;
    -webkit-border-radius: 35px;
    -moz-border-radius: 35px;
    border-radius: 35px;
    display: none;
    -webkit-transition: all 0.3s linear;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
#return-to-top i {
    color: #fff;
    margin: 0;
    position: relative;
    left: 16px;
    top: 13px;
    font-size: 19px;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
#return-to-top:hover {
    background: #8A090B;
} 
#return-to-top:hover i {
    color: #fff;
    top: 5px;
}
		  
		  .fa-chevron-up {
			  color: #f1f1f1;
		  }

		  .gallery img:hover {
			  opacity: 0.7;
		  }
				
</style>
</head> 
 
<body>
	
	<!--Used for Back-To-Top-->
	<a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up fa-2x"></i></a>

    <!--NAVIGATION-->
	<div class="container" style="width: 100%; color: #091747;">  
		<img src= "Images/J.W.Lewis_Construction_logo.jpg" alt="J.W. Lewis Construction" height="100" width="100"> 
	  <ul class="nav justify-content-end">  
	  <li class="nav-item">
            <a class="nav-link active" href="/">Home</a> 
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about">About</a>
          </li>
          <li class="nav-item"> 
            <a class="nav-link" href="services">Services</a> 
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery">Gallery</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact">Contact</a>
          </li> 
	  </ul>
	  </div> 
  
	<!--HEADER FRAME SECTION-->   
<div> 
		<img src="Images/breadcrumb_II.png" width="100%" height="120px" style="margin-top: 30px;"></div>
		
		<div class="contact-header" style="margin-top: 70px;">
		<h1 style="text-align: center;">J.W. LEWIS GALLERY</h1></div> 
		<div class="contact-header"><h4 style="text-align: center; color: #8A090B;">Take a look at all of the detail.</h4></div>
	  	<div class="houzz-logo"><a href="https://www.houzz.com/pro/dlew520/jw-lewis-construction"><img src="Images/J.W.Lewis_Construction_Houzz_logo_II.png" height="15%" width="18%" style="float: right; margin-top: -100px; padding-right: 15px;"></a></div>

		 
		</div>

	 <!-- Start your project here-->
  <!-- Grid row -->
  <div class="container-fluid" style="padding-top: 2%;">
    <div class="row">

        <!-- Grid column -->
        <div align="center" class="col-md-12 d-flex justify-content-center mb-5">
      
          <button type="button" class="btn btn-outline-black waves-effect filter" data-rel="all">All</button> 
          <button type="button" class="btn btn-outline-black waves-effect filter" data-rel="1">Interior</button>
          <button type="button" class="btn btn-outline-black waves-effect filter" data-rel="2">Exterior</button>
      
        </div>
        <!-- Grid column -->
      
      </div>
   </div>          
		  <!-- Grid row -->
          <div class="gallery" id="gallery" style="padding-top: 2%; width: 90%; padding-left: 10%; padding-bottom: 5%;">
		  
			<!--INTERIOR IMAGES-->
            <!-- Grid column -->
            <div class="mb-3 pics animation all 1">
            	<a href="Images/kitchen_remodel_resize_1.png" data-lightbox="kitchen remodel #2"><img  src="Images/kitchen_remodel(1).png" alt="Kitchen remodel"></a>
            </div>
            <!-- Grid column -->
          
            <!-- Grid column -->
            <div class="mb-3 pics animation all 1">
				<a href="Images/kitchen_remodel_resize_2.png" data-lightbox="kitchen remodel #2"><img  src="Images/kitchen_remodel(2).png" alt="Kitchen remodel"></a>
            </div>
            <!-- Grid column -->
          
            <!-- Grid column -->
            <div class="mb-3 pics animation all 1">
				<a href="Images/kitchen_remodel_resize_3.png" data-lightbox="kitchen remodel #2"><img  src="Images/kitchen_remodel(3).png" alt="Kitchen remodel"></a>
            </div> 
            <!-- Grid column -->
          
            <!-- Grid column -->
            <div class="mb-3 pics animation all 1">
				<a href="Images/kitchen_remodel_resize_4.png" data-lightbox="kitchen remodel #2"><img  src="Images/kitchen_remodel(4).png" alt="Kitchen remodel"></a>
            </div>
            <!-- Grid column -->
          
            <!-- Grid column -->
            <div class="mb-3 pics animation all 1">
				<a href="Images/kitchen_remodel_resize_5.png" data-lightbox="kitchen remodel #2"><img  src="Images/kitchen_remodel(5).png" alt="Kitchen remodel"></a>
            </div>
            <!-- Grid column -->
          
            <!-- Grid column -->
            <div class="mb-3 pics animation all 1">
				<a href="Images/kitchen_remodel_resize_6.png" data-lightbox="kitchen remodel #2"><img  src="Images/kitchen_remodel(6).png" alt="Kitchen remodel"></a>
            </div>
            <!-- Grid column -->
			<div class="mb-3 pics animation all 1">
				<a href="Images/kitchen_remodel_resize_7.png" data-lightbox="kitchen remodel #2"><img  src="Images/kitchen_remodel(7).png" alt="Kitchen remodel"></a>
			</div>
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
				<a href="Images/kitchen_remodel_resize_8.png" data-lightbox="kitchen remodel #2"><img  src="Images/kitchen_remodel(8).png" alt="Kitchen remodel"></a>
			</div>
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
				<a href="Images/Victorian_Railing(1).png" data-lightbox="19th Century Victorian Railing/Bathroom Vanities"><img  src="Images/Victorian_railing_resize(1).png" alt="Victorian Railing"></a> 
			</div>
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
				<a href="Images/Victorian_Railing(2).png" data-lightbox="19th Century Victorian Railing/Bathroom Vanities"><img  src="Images/Victorian_railing_resize(2).png" alt="Victorian Railing"></a>  
			</div>
			
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
				<a href="Images/Victorian_Railing(4).png" data-lightbox="19th Century Victorian Railing/Bathroom Vanities"><img  src="Images/Victorian_railing_resize(4).png" alt="Victorian Railing"></a> 
			</div>
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
		<a href="Images/Bathroom_vanities_resize_1.png" data-lightbox="Bathroom Vanities"><img  src="Images/Bathroom_Vanities(1).png" alt="Bathroom Vanities"></a>
			</div>
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
		<a href="Images/Bathroom_vanities_resize_2.png" data-lightbox="Bathroom Vanities"><img  src="Images/Bathroom_Vanities(2).png" alt="Bathroom Vanities"></a>
			</div>
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
		<a href="Images/Bathroom_vanities_resize_3.png" data-lightbox="Bathroom Vanities"><img  src="Images/Bathroom_Vanities(3).png" alt="Bathroom Vanities"></a>
			</div>
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
		<a href="Images/Bathroom_vanities_resize_4.png" data-lightbox="Bathroom Vanities"><img  src="Images/Bathroom_Vanities(4).png" alt="Bathroom Vanities"></a> 
			</div>


			<!--Timbertech Deck Grid Column-->

			 <!-- Grid column -->
				 <div class="mb-3 pics animation all 2">
					<a href="Images/Timbertech_deck_resize_8.png" data-lightbox="Timbertech Deck"><img src="Images/Timbertech_deck(8).png" alt="Timbertech Deck"></a> 
				</div>
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 2">
				<a href="Images/Timbertech_deck_resize_1.png" data-lightbox="Timbertech Deck"><img src="Images/Timbertech_deck(1).png" alt="Timbertech Deck"></a> 
			</div>
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 2">
				<a href="Images/Timbertech_deck_resize_2.png" data-lightbox="Timbertech Deck"><img src="Images/Timbertech_deck(2).png" alt="Timbertech Deck"></a> 
			</div>
		
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 2">
				<a href="Images/Timbertech_deck_resize_4.png" data-lightbox="Timbertech Deck"><img src="Images/Timbertech_deck(4).png" alt="Timbertech Deck"></a> 
			</div>
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 2">
				<a href="Images/Timbertech_deck_resize_5.png" data-lightbox="Timbertech Deck"><img src="Images/Timbertech_deck(5).png" alt="Timbertech Deck"></a> 
			</div>
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 2">
				<a href="Images/Timbertech_deck_II_resize_1.png" data-lightbox="Timbertech Deck"><img src="Images/Timbertech_deck_II(1).png" alt="Timbertech Deck"></a> 
			</div>
						
		<!--Wheelchair-accessible Bathroom Remodel Grid Column-->

			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
				<a href="Images/Wheelchair_accessible_resize(1).png" data-lightbox="Wheelchair accessible"><img src="Images/Wheelchair_accessible(1).png" alt="Wheelchair accessible bathroom remodel"></a> 
			</div>
			
			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
				<a href="Images/Wheelchair_accessible_resize(2).png" data-lightbox="Wheelchair accessible"><img src="Images/Wheelchair_accessible(2).png" alt="Wheelchair accessible bathroom remodel"></a> 
			</div>

			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
				<a href="Images/Wheelchair_accessible_resize(3).png" data-lightbox="Wheelchair accessible"><img src="Images/Wheelchair_accessible(3).png" alt="Wheelchair accessible bathroom remodel"></a> 
			</div>

			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
				<a href="Images/Wheelchair_accessible_resize(4).png" data-lightbox="Wheelchair accessible"><img src="Images/Wheelchair_accessible(4).png" alt="Wheelchair accessible bathroom remodel"></a> 
			</div>

			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
				<a href="Images/Wheelchair_accessible_resize(5).png" data-lightbox="Wheelchair accessible"><img src="Images/Wheelchair_accessible(5).png" alt="Wheelchair accessible bathroom remodel"></a> 
			</div>

			 <!-- Grid column -->
			 <div class="mb-3 pics animation all 1">
				<a href="Images/Wheelchair_accessible_resize(6).png" data-lightbox="Wheelchair accessible"><img src="Images/Wheelchair_accessible(6).png" alt="Wheelchair accessible bathroom remodel"></a> 
			</div>

		<!--Front Porch Renovation Grid Column-->
			
			 <!-- Grid column -->
			<div class="mb-3 pics animation all 2">
				<a href="Images/Timbertech_deck_resize_9.png"  data-lightbox="Timbertech Deck II/Front Porch Renovation"><img src="Images/Timbertech_deck(9).png" alt="Timbertech Deck"></a> 
			</div>

			<!-- Grid column -->
			<div class="mb-3 pics animation all 2">
				<a href="Images/Timbertech_deck_resize_6.png"  data-lightbox="Timbertech Deck II/Front Porch Renovation"><img src="Images/Timbertech_deck(6).png" alt="Timbertech Deck"></a> 
			</div>

			<!-- Grid column -->
			<div class="mb-3 pics animation all 2">
				<a href="Images/Timbertech_deck_resize_7.png"  data-lightbox="Timbertech Deck II/Front Porch Renovation"><img src="Images/Timbertech_deck(7).png" alt="Timbertech Deck"></a> 
			</div>

			<!-- Grid column -->
			<div class="mb-3 pics animation all 2">
			<a href="Images/Front_porch_renovation_resize_1.png" data-lightbox="Timbertech Deck II/Front Porch Renovation"><img  src="Images/Front_porch_renovation(1).png" alt="Front Porch Renovation"></a>  
			</div>

			<!-- Grid column -->
			<div class="mb-3 pics animation all 2">
			<a href="Images/Front_porch_renovation_resize_2.png" data-lightbox="Timbertech Deck II/Front Porch Renovation"><img  src="Images/Front_porch_renovation(2).png" alt="Front Porch Renovation"></a>  
			</div>

			<!-- Grid column -->
			<div class="mb-3 pics animation all 2">
			<a href="Images/Front_porch_renovation_resize_3.png" data-lightbox="Timbertech Deck II/Front Porch Renovation"><img  src="Images/Front_porch_renovation(3).png" alt="Front Porch Renovation"></a>  
			</div> 


		<!--Galley Kitchen Remodel-->

		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Galley_kitchen_remodel_resize.png" data-lightbox="Galley Kitchen"><img  src="Images/Galley_kitchen_remodel(1).png" alt="Kitchen remodel"></a> 
			</div> 

		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
				<a href="Images/Galley_kitchen_remodel_resize_2.png" data-lightbox="Galley Kitchen"><img  src="Images/Galley_kitchen_remodel(2).png" alt="Kitchen remodel"></a> 
				</div> 

		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
				<a href="Images/Galley_kitchen_remodel_resize_3.png" data-lightbox="Galley Kitchen"><img  src="Images/Galley_kitchen_remodel(3).png" alt="Kitchen remodel"></a> 
				</div> 
		
		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Galley_kitchen_remodel_resize_4.png" data-lightbox="Galley Kitchen"><img  src="Images/Galley_kitchen_remodel(4).png" alt="Kitchen remodel"></a> 
			</div> 
		
		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Galley_kitchen_remodel_resize_5.png" data-lightbox="Galley Kitchen"><img  src="Images/Galley_kitchen_remodel(5).png" alt="Kitchen remodel"></a> 
			</div> 
		

		<!--Master Bathroom Suite-->
		
		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Master_bathroom_suite_resize_1.png" data-lightbox="Master Bathroom"><img  src="Images/Master_bathroom_suite(1).png" alt="Bathroom remodel"></a>
			</div> 

		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Master_bathroom_suite_resize_2.png" data-lightbox="Master Bathroom"><img  src="Images/Master_bathroom_suite(2).png" alt="Bathroom remodel"></a>
			</div> 

		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Master_bathroom_suite_resize_4.png" data-lightbox="Master Bathroom"><img  src="Images/Master_bathroom_suite(4).png" alt="Bathroom remodel"></a>
			</div> 
		
		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Master_bathroom_suite_resize_5.png" data-lightbox="Master Bathroom"><img  src="Images/Master_bathroom_suite(5).png" alt="Bathroom remodel"></a>
			</div> 

		
		<!--Bathroom Remodel #2--> 
		
		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Bathroom_remodel_resize_1.png" data-lightbox="Bathroom Remodel #2"><img  src="Images/Bathroom_remodel(1).png" alt="Bathroom Remodel #2"></a>  
			</div> 
		
		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Bathroom_remodel_resize_2.png" data-lightbox="Bathroom Remodel #2"><img  src="Images/Bathroom_remodel(2).png" alt="Bathroom Remodel #2"></a>  
			</div> 
		
		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Bathroom_remodel_resize_3.png" data-lightbox="Bathroom Remodel #2"><img  src="Images/Bathroom_remodel(3).png" alt="Bathroom Remodel #2"></a>  
			</div> 

		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Bathroom_remodel_resize_4.png" data-lightbox="Bathroom Remodel #2"><img  src="Images/Bathroom_remodel(4).png" alt="Bathroom Remodel #2"></a>  
			</div> 

		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Bathroom_remodel_resize_5.png" data-lightbox="Bathroom Remodel #2"><img  src="Images/Bathroom_remodel(5).png" alt="Bathroom Remodel #2"></a>  
			</div> 


		<!--Master Bathroom Remodel-->

		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Master_bathroom_remodel_resize_1.png" data-lightbox="Master Bathroom Remodel"><img  src="Images/Master_bathroom_remodel(1).png" alt="Master Bathroom remodel"></a>
			</div> 

		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Master_bathroom_remodel_resize_3.png" data-lightbox="Master Bathroom Remodel"><img  src="Images/Master_bathroom_remodel(3).png" alt="Master Bathroom remodel"></a> 
			</div> 

		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Master_bathroom_remodel_resize_4.png" data-lightbox="Master Bathroom Remodel"><img  src="Images/Master_bathroom_remodel(4).png" alt="Master Bathroom remodel"></a>
			</div> 

		<!-- Grid column -->
		<div class="mb-3 pics animation all 1">
			<a href="Images/Master_bathroom_remodel_resize_5.png" data-lightbox="Master Bathroom Remodel"><img  src="Images/Master_bathroom_remodel(5).png" alt="Master Bathroom remodel"></a>
			</div> 
		  
		 

		  </div>
		  </div>
		  
		  	
	
       <!-- Include Footer section -->
	   <?php
    include 'partials/footer.php';  

        ?>
        <!-- End of Footer section -->

<!--Scripts-->
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script> 
<script type="text/javascript" src="JS/lightbox.js"></script> 
<script  src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> 

	


	   <script>
      function accFunction(id) {
          var x = document.getElementById(id);
          if (x.className.indexOf("w3-show") == -1) {
              x.className += " w3-show";
          } else {
              x.className = x.className.replace(" w3-show", "");
          }
      }
      </script>
	<script>
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    } 
}
</script>

<script>
$(function() {
    var selectedClass = "";
    $(".filter").click(function(){
    selectedClass = $(this).attr("data-rel");
    $("#gallery").fadeTo(100, 0.1);
    $("#gallery div").not("."+selectedClass).fadeOut().removeClass('animation');
    setTimeout(function() {
    $("."+selectedClass).fadeIn().addClass('animation');
    $("#gallery").fadeTo(300, 1);
    }, 300);
    });
    });

	$(function () {
$("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");
});
	</script>


	  <script>// ===== Scroll to Top ==== 
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});
</script>

	
</body>
</html>












