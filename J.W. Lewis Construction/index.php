<!DOCTYPE html>
<html lang="en">
  <head> 
 
    <!--Required Meta Tags-->  
    <meta charset="utf-8"> 
	  <meta name="viewport" content="width=device-width, initial-scale=1">  
    <meta name="google-site-verification" content="wp9cMpNhJanEoWVsTqxrUqC1jQ_iSqNEYa1KuG0u-98" />  
    
	  <title> J.W. Lewis Construction | Home  </title> 
    <link rel="shortcut icon" type="image/jpg" href="Images/J.W.Lewis_Construction_logo.jpg"> 
    
    <!--Bootstrap and Main CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  -->
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="CSS/lightbox.css"/> 
    <link rel="stylesheet" href="CSS/style.css"> 

    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto%7CJosefin+Sans:100,300,400,500" 
    rel="stylesheet" type="text/css"> 

	
  </head>  
	
	
	
  <body>

  <!--Used for Back-To-Top-->
  <a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up fa-2x"></i></a> 

  <!-- NAVIGATION -->
  <!-- <div class="container" style="width: 100%; color: #091747;">  
    <img src= "Images/J.W.Lewis_Construction_logo.jpg" class="logo-mobile" alt="J.W. Lewis Construction" height="100" width="100"> 
  <ul class="nav justify-content-end">  
    <li class="nav-item">
      <a class="nav-link active" href="index.html">Home</a> 
    </li>
    <li class="nav-item">
      <a class="nav-link" href="about.html">About</a>
    </li>
    <li class="nav-item"> 
      <a class="nav-link" href="services.html">Services</a> 
    </li>
    <li class="nav-item">
      <a class="nav-link" href="gallery.html">Gallery</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="contact.html">Contact</a>
    </li> 
  </ul>
  </div>   -->
  <nav class="navbar fixed-top">
    <div class="container-fluid">
     <a href="index.html"><img src= "Images/J.W.Lewis_Construction_logo.jpg" class="logo-mobile" alt="J.W. Lewis Construction" height="100" width="100"></a> 
      <form class="d-flex">
        <ul class="nav justify-content-end">  
          <li class="nav-item">
            <a class="nav-link active" href="/">Home</a> 
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about">About</a>
          </li>
          <li class="nav-item"> 
            <a class="nav-link" href="services">Services</a> 
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery">Gallery</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact">Contact</a>
          </li> 
        </ul>
      </form>
    </div>
  </nav>


  <!--HEADER FRAME SECTION-->
  <div id="breadcrumb-mobile" style="margin-top: 8%;"> 
		<img src="Images/breadcrumb_II.png" width="100%" height="120px"></div>
    <div class="houzz-logo"><a href="https://www.houzz.com/pro/dlew520/jw-lewis-construction"><img src="Images/J.W.Lewis_Construction_Houzz_logo_II.png" height="15%" width="18%" style="float: right;"></a></div>
    
  
	  
    
    <section id="showcase">  
      <div class="container-fluid">  
        <div class="row" >
          <div class="col-md-6 col-sm-6" >
            <div class="showcase-left">
              <img src="Images/Timbertech_deck_resize_1.png" width="100%" height="100%" alt="J.W. Lewis Construction Main Image">    
            </div>      
          </div>  
          <div class="col-md-6 col-sm-6"  style="font-size:20px;
  font-family: 'Josefin Sans';
  color:#000;">
            <div class="showcase-right">
              <h1>J.W. Lewis Construction</h1>
			  <h3 style="color: #8A090B;">Providing high quality remodeling for over 25 years!</h3>
              <p>We believe that our each of our client's tastes and needs are distinctive. Bearing this in mind, our work is customized and tailored specifically to fit each project. We use the best building materials and products on the market and do our best to try to minimize the impact of the construction process to the home. Our goal is to bring your ideas to life, while increasing your level of comfort in your home. </p>
			
			
            </div>   
            <br>   
        
          </div> 
        </div>
      </div>

    </section>


	
   <!-- About Section -->
    
 <div class="container-fluid" style="background-color:#091747;">
		    <section  id="info1" class="info-left" style="background-color:#091747;"> 
      <div> 
        <div class="row">
          <div class="col-md-6 col-sm-6"> 
            <div class="info-left">
              <img src="Images/J.W.Lewis_Construction_about_III.jpg" height="100%" width="100%" style=" opacity: 1;"> 
            </div>
          </div> 
          <div class="col-md-6 col-sm-6" style=" font-size:20px;
  font-family: 'Josefin Sans'; 
  color:#000;">  
            <div class="info-right" style="color: white; margin-top: 15%;">
				
              <h2 style="color: white;">ABOUT J.W. LEWIS</h2>
				
              <p style="color: white;">Incorporated in 1989, J.W. Lewis Construction has been providing the central New Jersey area with high quality, turn-key remodeling projects and home renovations for over 25 years! Our main asset is our extremely loyal client base, which is the foundation of the company. Over and over again, our clients have called on us to complete a wide range of projects, both large and small.</p> 
				
				<p style="color: white;">J.W. Lewis Construction is family owned and operated. You will always have the owner or member of the family on your jobsite, working directly on your project. We will patiently guide you from the planning and design phase, through the construction process and project completion.
	You will receive a professional result, completed within...</p>
             <br>
	
<a href="About.html"><button class="button" style="vertical-align:middle; color: black;"><span>Read More </span></button></a>
			 
            </div> 
          </div>
        </div> 
		
      </div>
    </section>
  </div>

<!--TESTIMONIAL SECTION-->
  <div id="testimonial" class="carousel slide testimonial" data-ride="carousel" style="padding-top: 10%; padding-bottom: 10%; width: 100%; background-color: #E3E3E3;">
    <ol class="carousel-indicators">
      <li data-target="#testimonial" data-slide-to="0" class="active"></li>
      <li data-target="#testimonial" data-slide-to="1"></li>
      <li data-target="#testimonial" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active"> 
        <p class="container">"J.W. Lewis Construction have been my "go to" company for projects around the house for close to 20 years. We were always impressed with John's willingness to invest a lot of time trying to understand exactly the customer's needs, including trying out sample paints and the like. Years after a project is completed, John continues to come for small repairs and other needs that may arise over time."</p> 
        <p class="customer">Alex H.</p>
      </div>
      <div class="carousel-item">
        <p class="container">"John and his group are a pleasure to work with. They will start when all materials are in at the promised date and time. All of them are very professional and work with electricians and plumbers that share their punctuality and work ethic. John and his workers invest ample time in their project. They have remodeled three of our bathrooms, painted the house and done several other repairs and improvements. All of our construction jobs go to him!"</p> 
        <p class="customer">Buhler J.</p>
      </div>
      <div class="carousel-item">
        <p class="container">"As a property manager, I have used J.W. Lewis on several of my sites. The work is always done in a professional manner. John and his crew perform outstanding work and work in a professional manner. They are respectful and happy to answer any questions. Completed projects are aesthetically pleasing, the work site is always left neat and tidy. I will continue to use John and his crew and highly recommend them for all types of construction projects."</p> 
        <p class="customer">Kelly B.</p>
      </div>
    </div>
    <a class="carousel-control-prev" href="#testimonial" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#testimonial" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

		
	<!--GALLERY SECTION		 
      <div style="min-width: 100%;"> 
      <div class="gallery-left" >  
      <a href="Gallery.html"><img src="Images/Gallery_home_image_IIII.png" width="100%" height="100%" style="margin-top: -1%; ">   
			 </a>    
      </div>  
      </div> -->
        
      
        <div class="services-image-I gallery-left">  
        
                <img src="Images/Gallery_home_image_IIII.png" 
        class="services-image" border="0" width="100%" 
        height="100%" alt="Interior/Exterior Image">      
        </div>    
         
		 
 
<!--Form Section--> 

<form class="container contact-form" style="padding-top: 5%; padding-bottom: 5%;"
action="form.php" method="post" >
  <h1>Contact Us</h1>
  <h3 style="color: #8A090B">We'd love to hear your ideas!</h3>
  <br>
  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp" 
    placeholder="Please enter your name...">
  </div>
  <div class="form-group">
    <label for="email">Email Address</label>
    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp"
     placeholder="Please enter your email...">
  </div>
  <div class="form-group"> 
    <label for="message">Message</label>
    <input type="text" class="form-control" id="message" name="message" aria-describedby="emailHelp" 
    placeholder="Tell us about your ideas..." style="height: 200px;">
  </div>
  <input type="submit" value="Submit">
</form>

<!--Footer Section-->
	
	     <!-- <footer class="w3-padding-xlarge w3-center" style="width: 100%; height: 100%; margin-top: 0px; background-color: #091747; color: white;">
      <p style="text-align: center;">J.W. Lewis Construction &copy; 2022, All Rights Reserved</p>
			 <div style="padding: 10px 0 10 10;">
				 
		<div class="contact-email" style="text-align: center; padding-right: 120px;">
      <a href="mailto: jwlewis2247@gmail.com"><i class="fas fa-envelope fa-2x" style="color: white;"></i></a></div> 
				
	
	  <div class="contact-linkedin" style="text-align: center; margin-top: -2px; margin-top: -55px; "> <a href="https://www.linkedin.com/in/john-lewis-9183b32a"><i class="fab fa-linkedin-in fa-2x" style="color: white;"></i> </a></div> 
			 
	  <div style=" padding-left: 120px; margin-top: -55px; text-align: center;">
	   <a href="Tel: 7324221098" style="color: white;"><i class="fas fa-phone fa-2x"></i></a></div>  
	 </div>	   

    </footer> -->

        <!-- Include Footer section -->
              <?php
    include 'partials/footer.php';  

        ?>
        <!-- End of Footer section -->
		 
	  <!--Scripts-->
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" 
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="JS/lightbox.js"></script> 
    <script  src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>  
    <script src="https://unpkg.com/scrollreveal@4.0.0/dist/scrollreveal.min.js"></script> 
    <!--<script src="https://unpkg.com/scrollreveal"></script>-->
    <!--<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>-->
    <script src="script.js"></script>
    
    <!--Scroll Reveal JS-->
  
    <script>
		window.sr = ScrollReveal();
        sr.reveal('.navbar', {
          duration: 2000,
          origin:'bottom'
        });
		sr.reveal('.houzz-logo', {
          duration: 2000,
          origin:'top',
          distance:'300px'
        });
        sr.reveal('.showcase-left', {
          duration: 2000,
          origin:'top',
          distance:'300px'
        });
        sr.reveal('.showcase-right', {
          duration: 2000,
          origin:'left',
          distance:'200px'
        });
        sr.reveal('.showcase-btn', {
          duration: 2000,
          delay: 2000,
          origin:'top'
        });
        sr.reveal('.testimonial', {
          duration: 3000,
          origin:'left'
        });
		sr.reveal('.gallery-left', {
          duration: 2000,
          origin:'left',
          distance:'400px',
          viewFactor: 0.2
        });
		
		sr.reveal('.breadcrumb_III', {
          duration: 2000,
          origin:'left',
          distance:'400px',
          viewFactor: 0.2
        });
     
        sr.reveal('.gallery-right', {
          duration: 2000,
          origin:'left',
          distance:'300px',
          viewFactor: 0.2
        });
		 sr.reveal('.contact-form', {
          duration: 3000,
          origin:'top', 
          distance:'500px',
          viewFactor: 0.2 
        });
		 sr.reveal('.info-right', {
          duration: 2000,
          origin:'bottom',
          distance:'300px',
          viewFactor: 0.2
        });
		  sr.reveal('.info-left', {
          duration: 2000,
          origin:'bottom',
	      distance: '300px',
		  viewFactor: 0.2
        });
        
    </script>

  
	    <script>
      function accFunction(id) {
          var x = document.getElementById(id);
          if (x.className.indexOf("w3-show") == -1) {
              x.className += " w3-show";
          } else {
              x.className = x.className.replace(" w3-show", "");
          }
      }
      </script>
   
   <!--Scroll To Top-->
	  <script> 
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});
</script>

  </body>
</html>
